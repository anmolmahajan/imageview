package com.cuddleinstyle.imageshifter;

import android.content.Context;
import android.content.DialogInterface;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button buttonInc = (Button) findViewById(R.id.inc);
        Button buttonDec = (Button) findViewById(R.id.dec);
        Button buttonChange = (Button) findViewById(R.id.change);


        buttonChange.setOnClickListener(this);
        buttonDec.setOnClickListener(this);
        buttonInc.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //ImageView iv = (ImageView) findViewById(R.id.iv);
    float a = 1f;
    int i = 2;
    @Override
    public void onClick(View v) {
        CharSequence str = "";
        ImageView iv = (ImageView) findViewById(R.id.iv);
        if(v.getId() == R.id.inc){
            a = a - 0.15f;
            str = str + "Increment";
        }

        if(v.getId() == R.id.dec){
            a = a + 0.15f;
            str = str + "Decrement";
        }

        if(v.getId() == R.id.change){
            switch(i){
                case 1:
                    i = 2;
                    iv.setImageResource(R.drawable.dp);
                    break;
                case 2:
                    i = 1;
                    iv.setImageResource(R.drawable.fr);
                    break;
            }
            str = str + "Change";
        }
        iv.setAlpha(a);

        Context context = getApplicationContext();
        CharSequence text = "You clicked " + str;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

}
